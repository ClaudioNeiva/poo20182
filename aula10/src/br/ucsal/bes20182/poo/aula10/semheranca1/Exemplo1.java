package br.ucsal.bes20182.poo.aula10.semheranca1;

public class Exemplo1 {

	public static void main(String[] args) {

		Pessoa pessoaFisica = new Pessoa();
		pessoaFisica.nome = "Claudio";
		pessoaFisica.cpf = "123213";
		// Pessoas f�sicas n�o podem ter inscri��o estadual
		pessoaFisica.inscricaoEstadual = 546756;

		Pessoa pessoaJuridica = new Pessoa();
		// Pessoas jur�dicas n�o podem ter cpf
		pessoaJuridica.cpf = "565756";

	}

}
