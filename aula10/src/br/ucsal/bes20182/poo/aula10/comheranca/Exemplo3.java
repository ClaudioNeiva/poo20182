package br.ucsal.bes20182.poo.aula10.comheranca;

import java.util.ArrayList;
import java.util.List;

public class Exemplo3 {

	public static void main(String[] args) {

		List<Pessoa> clientes = new ArrayList<>();

		PessoaFisica pessoaFisica1 = new PessoaFisica();
		pessoaFisica1.nome = "Claudio";
		pessoaFisica1.cpf = "254";
		pessoaFisica1.logradouro = "Rua x";

		PessoaJuridica pessoaJuridica1 = new PessoaJuridica();
		pessoaJuridica1.nome = "Empresa 1";
		pessoaJuridica1.cnpj = "4590";
		pessoaJuridica1.logradouro = "Rua y";

		clientes.add(pessoaFisica1);
		clientes.add(pessoaJuridica1);

		Pessoa pessoa1 = new PessoaFisica();
		pessoa1.nome = "Caju";

		System.out.println(clientes.get(0).nome);

		// O c�digo abaixo n�o compila, pois Pessoa foi definida como abstract
		Pessoa pessoa2 = new Pessoa();

	}

}
