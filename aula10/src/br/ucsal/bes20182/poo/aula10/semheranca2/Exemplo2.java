package br.ucsal.bes20182.poo.aula10.semheranca2;

import java.util.ArrayList;
import java.util.List;

public class Exemplo2 {

	public static void main(String[] args) {

		List<PessoaFisica> clientesPessoaFisica = new ArrayList<>();
		List<PessoaJuridica> clientesPessoaJuridica = new ArrayList<>();
		
		PessoaFisica pessoaFisica = new PessoaFisica();
		pessoaFisica.nome = "Claudio";
		pessoaFisica.cpf = "123213";

		PessoaJuridica pessoaJuridica = new PessoaJuridica();
		pessoaJuridica.nome = "Empresa 1";
		pessoaJuridica.cnpj = "98789789";
	}
}
