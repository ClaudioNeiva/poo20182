package br.ucsal.bes20182.poo.aula02;

public class Exemplo01 {

	public static void main(String[] args) {
		
		Aluno aluno1 = new Aluno();
		aluno1.nome = "Claudio Neiva";

		Aluno aluno2 = new Aluno();
		aluno2.nome = "Maria";

		System.out.println("aluno1=" + aluno1);
		System.out.println("aluno2=" + aluno2);

		System.out.println("aluno1.nome=" + aluno1.nome);
		System.out.println("aluno2.nome=" + aluno2.nome);

	}

}
