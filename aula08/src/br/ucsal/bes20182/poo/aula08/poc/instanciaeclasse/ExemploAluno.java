package br.ucsal.bes20182.poo.aula08.poc.instanciaeclasse;

public class ExemploAluno {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno("Claudio");
		Aluno aluno2 = new Aluno("Maria");

		System.out.println("Aluno1.Nome="+aluno1.getNome());
		System.out.println("Aluno1.Matricula="+aluno1.getMatricula());
		System.out.println("Aluno2.Nome="+aluno2.getNome());
		System.out.println("Aluno2.Matricula="+aluno2.getMatricula());
		
	}

}
