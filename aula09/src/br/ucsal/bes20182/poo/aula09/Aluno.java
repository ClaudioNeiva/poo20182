package br.ucsal.bes20182.poo.aula09;

public class Aluno {

	private static Integer contador = 0;

	private Integer matricula;

	private String nome;

	private String email;

	public Aluno() {
		definirMatricula();
	}

	private void definirMatricula() {
		contador++;
		this.matricula = contador;
	}

	// public static Integer getContador() {
	// return contador;
	// }
	//
	// public static void setContador(Integer contador) {
	// matricula = 20; // Isso N�O funciona!!!!
	// Aluno.contador = contador;
	// }

	public Integer getMatricula() {
		return matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
