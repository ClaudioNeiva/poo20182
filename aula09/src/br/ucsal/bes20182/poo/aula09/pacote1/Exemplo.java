package br.ucsal.bes20182.poo.aula09.pacote1;

import br.ucsal.bes20182.poo.aula09.Aluno;

public class Exemplo {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno();
		Aluno aluno2 = new Aluno();
		Aluno aluno3 = new Aluno();
	
		//Aluno.setContador(10);
		
		System.out.println(aluno1);
		System.out.println(aluno2);
		System.out.println(aluno3);
		
	}

}
