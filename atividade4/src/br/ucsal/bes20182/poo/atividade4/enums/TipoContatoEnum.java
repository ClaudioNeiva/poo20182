package br.ucsal.bes20182.poo.atividade4.enums;

public enum TipoContatoEnum {
	PESSOAL, PROFISSIONAL, OUTROS;
	
	public static String obterItens() {
		String itens = "";
		for (TipoContatoEnum tipo : TipoContatoEnum.values()) {
			itens += tipo + ",";
		}
		return itens.substring(0, itens.length() - 1);
	}

}
