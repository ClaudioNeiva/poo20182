package br.ucsal.bes20182.poo.atividade4.tui;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

import br.ucsal.bes20182.poo.atividade4.business.ContatoBO;
import br.ucsal.bes20182.poo.atividade4.domain.Contato;
import br.ucsal.bes20182.poo.atividade4.enums.TipoContatoEnum;
import br.ucsal.bes20182.poo.atividade4.util.ScannerUtil;

public class ContatoTui {

	public static Scanner scanner = new Scanner(System.in);

	public ContatoBO contatoBO = new ContatoBO();

	public void incluir() {
		System.out.println("############ INCLUS�O DE CONTATOS ############");
		String nome = ScannerUtil.obterTexto("Informe o nome:");
		String telefone = ScannerUtil.obterTexto("Infome o telefone:");
		Integer anoNascimento = ScannerUtil.obterInteiro("Informe o ano de nascimento:");
		Date dataNascimento = ScannerUtil.obterData("Informe a data de nascimento(dia/m�s/ano):");
		TipoContatoEnum tipoContato = obterTipoContato();
		Contato contato = new Contato(nome, telefone, anoNascimento, dataNascimento, tipoContato);
		String erro = contatoBO.incluir(contato);
		if (erro != null) {
			System.out.println("N�o foi poss�vel criar o contato.");
			System.out.println(erro);
		}
	}

	private TipoContatoEnum obterTipoContato() {
		while (true) {
			System.out.println("Informe o tipo do contato (" + TipoContatoEnum.obterItens() + "):");
			String tipoString = scanner.nextLine();
			try {
				TipoContatoEnum tipoContato = TipoContatoEnum.valueOf(tipoString.toUpperCase());
				return tipoContato;
			} catch (IllegalArgumentException e) {
				System.out.println("O tipo informado n�o � v�lido.");
			}
		}
	}

	public void listar() {
		System.out.println("############ LISTAGEM DE CONTATOS ############");
		List<Contato> contatos = contatoBO.obterTodos();

		for (Contato contato : contatos) {
			System.out.println(" Contato ");
			System.out.println(" \t" + contato.getNome());
			System.out.println(" \tTelefone=" + contato.getTelefone());
			System.out.println(" \tAno de nascimento=" + contato.getAnoNascimento());
			System.out.println(" \tData de nascimento=" + ScannerUtil.formatarData(contato.getDataNascimento()));
		}
	}

}
