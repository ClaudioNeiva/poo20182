package br.ucsal.bes20182.poo.atividade4.persistence;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.poo.atividade4.domain.Contato;

public class ContatoDAO {

	private static final String NOME_ARQUIVO = "c:\\trabalho\\contatos.dat";
	
	public List<Contato> contatos = new ArrayList<>();
	
	public ContatoDAO() {
		//lerContatosDisco();
	}

	public String incluir(Contato contato) {
		contatos.add(contato);
		gravarContatosDisco();
		return null;
	}

	private void gravarContatosDisco() {
		try {
			ObjectOutput objectOutput = new ObjectOutputStream(new FileOutputStream(NOME_ARQUIVO));
			objectOutput.writeObject(contatos);
			objectOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void lerContatosDisco() {
		try {
			ObjectInput objectInput = new ObjectInputStream(new FileInputStream(NOME_ARQUIVO));
			contatos = (List<Contato>) objectInput.readObject();
			objectInput.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<Contato> obterTodos() {
		return contatos;
	}

}
