package br.ucsal.bes20182.poo.atividade4.business;

import java.util.List;

import br.ucsal.bes20182.poo.atividade4.domain.Contato;
import br.ucsal.bes20182.poo.atividade4.persistence.ContatoDAO;

public class ContatoBO {

	public ContatoDAO contatoDAO = new ContatoDAO();
	
	public String incluir(Contato contato) {
		String erro = validar(contato);
		if(erro != null){
			return erro;
		}
		return contatoDAO.incluir(contato);
	}

	public List<Contato> obterTodos() {
		return contatoDAO.obterTodos();
	}

	private String validar(Contato contato) {
		if(contato.getNome().trim().isEmpty()){
			return "ERRO: nome do contato n�o informado.";
		}
		if(contato.getTelefone().trim().isEmpty()){
			return "ERRO: telefone do contato n�o informado.";
		}
		return null;
	}

}
