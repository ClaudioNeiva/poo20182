package br.ucsal.bes20182.poo.atividade4.domain;

import java.io.Serializable;
import java.util.Date;

import br.ucsal.bes20182.poo.atividade4.enums.TipoContatoEnum;

public class Contato implements Serializable {

	private static final long serialVersionUID = 2L;

	private String nome;

	private String telefone;

	private Integer anoNascimento;

	private Date dataNascimento;

	private TipoContatoEnum tipo;

	public Contato(String nome, String telefone, Integer anoNascimento, Date dataNascimento, TipoContatoEnum tipo) {
		this.nome = nome;
		this.telefone = telefone;
		this.anoNascimento = anoNascimento;
		this.dataNascimento = dataNascimento;
		this.tipo = tipo;
	}

	public TipoContatoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoContatoEnum tipo) {
		this.tipo = tipo;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

}
