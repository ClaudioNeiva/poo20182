package br.ucsal.bes20182.poo.aula03.exemplo1;

import java.util.Scanner;

public class LeituraTextoNumero {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Informe o texto 1:");
		String texto1 = scanner.nextLine();

		System.out.println("Informe o numero 1:");
		Integer numero1 = scanner.nextInt();
		scanner.nextLine(); // Fazemos um nextLine para remover o ENTER do
							// buffer do teclado.

		System.out.println("Informe o texto 2:");
		String texto2 = scanner.nextLine();

		System.out.println("Texto1=" + texto1);
		System.out.println("Numero1=" + numero1);
		System.out.println("Texto2=" + texto2);
	}

}
