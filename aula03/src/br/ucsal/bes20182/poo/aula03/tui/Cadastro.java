package br.ucsal.bes20182.poo.aula03.tui;

import br.ucsal.bes20182.poo.aula03.business.Contato;

public class Cadastro {

	public void cadastrarContato(){
		Contato contato1 = new Contato();
		contato1.validar();
		
		br.ucsal.bes20182.poo.aula03.persistence.Contato contato2 = new br.ucsal.bes20182.poo.aula03.persistence.Contato();
		contato2.inserir();
	}
	
}
