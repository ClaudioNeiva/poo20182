package br.ucsal.bes20182.poo.aula03.tui;

import br.ucsal.bes20182.poo.aula03.business.ContatoBO;
import br.ucsal.bes20182.poo.aula03.persistence.ContatoDAO;

public class Cadastro2 {

	public void cadastrarContato() {
		
		ContatoBO contatoBO = new ContatoBO();
		contatoBO.validar();
		
		ContatoDAO contatoDAO = new ContatoDAO();
		contatoDAO.inserir();
		
	}

}
