package br.ucsal.bes20182.poo.aula06.poc.list;

import java.util.ArrayList;
import java.util.List;

public class ListExemplo2 {

	public static void main(String[] args) {
		List<String> nomes = new ArrayList<>();

		nomes.add("Claudio");
		nomes.add("Maria");
		nomes.add("Caju");
		// N�o ser� poss�vel adcionar a lista objetos de tipos diferentes de
		// String.
		// nomes.add(new Date());

		for (int i = 0; i < nomes.size(); i++) {
			String nome = nomes.get(i);

			System.out.println(nome);
		}

		for (String nome : nomes) {
			System.out.println(nome);
		}
	}

}
