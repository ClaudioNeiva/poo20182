package br.ucsal.bes20182.poo.aula06.poc.list;

import java.util.ArrayList;
import java.util.List;

public class ListExemplo3 {

	public static void main(String[] args) {
		List<Pessoa> pessoas = new ArrayList<>();

		pessoas.add(new Pessoa("Claudio", 20));
		pessoas.add(new Pessoa("Maria", 19));
		pessoas.add(new Pessoa("Pedro", 40));
		pessoas.add(new Pessoa("Joaquim", 30));
		pessoas.add(new Pessoa("Joana", null));

		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa.toString());
		}

		Pessoa pessoa1 = new Pessoa("Joana", null);
		
		if (pessoas.contains(pessoa1)) {
			System.out.println("a pessoa1 est� na lista.");
		} else {
			System.out.println("a pessoa1 N�O est� na lista.");
		}
	}
}
