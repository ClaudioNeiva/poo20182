package br.ucsal.bes20182.poo.atividade9;

public class Caminhao extends Veiculo {

	private Integer qtdEixos;

	private Integer capacidadeCarga;

	public Caminhao(String placa, String modelo, Integer anoFabricacao, Double valor, Integer qtdEixos,
			Integer capacidadeCarga) throws ValorNaoValidoException {
		super(placa, modelo, anoFabricacao, valor);
		this.qtdEixos = qtdEixos;
		this.capacidadeCarga = capacidadeCarga;
	}

	@Override
	public Double calcularCustoLocacao() {
		return 8d * capacidadeCarga;
	}

	public Integer getQtdEixos() {
		return qtdEixos;
	}

	public void setQtdEixos(Integer qtdEixos) {
		this.qtdEixos = qtdEixos;
	}

	public Integer getCapacidadeCarga() {
		return capacidadeCarga;
	}

	public void setCapacidadeCarga(Integer capacidadeCarga) {
		this.capacidadeCarga = capacidadeCarga;
	}

	@Override
	public String toString() {
		return "Caminhao [qtdEixos=" + qtdEixos + ", capacidadeCarga=" + capacidadeCarga + ", toString()="
				+ super.toString() + "]";
	}

}
