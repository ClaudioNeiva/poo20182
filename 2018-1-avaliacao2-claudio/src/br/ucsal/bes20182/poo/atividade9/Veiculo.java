package br.ucsal.bes20182.poo.atividade9;

public abstract class Veiculo implements Comparable<Veiculo> {

	private String placa;

	private String modelo;

	private Integer anoFabricacao;

	private Double valor;

	public Veiculo(String placa, String modelo, Integer anoFabricacao, Double valor) throws ValorNaoValidoException {
		super();
		this.placa = placa;
		this.modelo = modelo;
		this.anoFabricacao = anoFabricacao;
		setValor(valor);
	}

	public abstract Double calcularCustoLocacao();

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) throws ValorNaoValidoException {
		validarValor(valor);
		this.valor = valor;
	}

	private void validarValor(Double valor) throws ValorNaoValidoException {
		if (valor <= 0) {
			throw new ValorNaoValidoException();
		}
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", modelo=" + modelo + ", anoFabricacao=" + anoFabricacao + ", valor="
				+ valor + "]";
	}

	@Override
	public int compareTo(Veiculo o) {
		return placa.compareTo(o.placa);
	}

}
