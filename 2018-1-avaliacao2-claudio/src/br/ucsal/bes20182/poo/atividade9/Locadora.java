package br.ucsal.bes20182.poo.atividade9;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Locadora {

	private static List<Veiculo> veiculos = new ArrayList<>();

	public static void cadastrarOnibus(String placa, String modelo, Integer anoFabricacao, Double valor,
			Integer qtdMaximaPassageiros) {
		try {
			Onibus onibus = new Onibus(placa, modelo, anoFabricacao, valor, qtdMaximaPassageiros);
			veiculos.add(onibus);
		} catch (ValorNaoValidoException e) {
			System.out.println("Valor inv�lido!");
		}
	}

	public static void cadastrarCaminhao(String placa, String modelo, Integer anoFabricacao, Double valor,
			Integer qtdEixos, Integer capacidadeCarga) {
		try {
			Caminhao caminhao = new Caminhao(placa, modelo, anoFabricacao, valor, qtdEixos, capacidadeCarga);
			veiculos.add(caminhao);
		} catch (ValorNaoValidoException e) {
			System.out.println("Valor inv�lido!");
		}
	}

	/**
	 * Listar a placa, valor e custo loca��o de cada ve�culo, por ordem
	 * crescente de valor;
	 */
	public static void listarVeiculosOrdenadoPorValor() {
		Collections.sort(veiculos, new Comparator<Veiculo>() {
			@Override
			public int compare(Veiculo o1, Veiculo o2) {
				return o1.getValor().compareTo(o2.getValor());
			}
		});
		System.out.println("Ve�culos ordenados por valor:");
		for (Veiculo veiculo : veiculos) {
			System.out.println("Placa: " + veiculo.getPlaca() + " | valor: " + veiculo.getValor()
					+ " | custo de loca��o: " + veiculo.calcularCustoLocacao());
		}
	}

	/**
	 * Listar os ve�culos (todos os dados) por ordem crescente de placa. Esta
	 * deve ser a ordena��o padr�o para ve�culos;
	 */
	public static void listarVeiculosOrdenadoPorPlaca() {
		Collections.sort(veiculos);
		System.out.println("Ve�culos ordenados por placa:");
		for (Veiculo veiculo : veiculos) {
			System.out.println(veiculo);
		}
	}

	/**
	 * Listar os modelos de ve�culo que a locadora possui. N�o devem ser
	 * exibidos nomes de modelo repetidos.
	 */
	public static void listarModelos() {
		//	List<String> modelosUsandoListaPorqueNaoEstudei = new ArrayList<>();
		//	for (Veiculo veiculo : veiculos) {
		//		if (!modelosUsandoListaPorqueNaoEstudei.contains(veiculo.getModelo())) {
		//			modelosUsandoListaPorqueNaoEstudei.add(veiculo.getModelo());
		//		}
		//	}

		Set<String> modelos = new HashSet<>();
		// modelos = new LinkedHashSet<>();
		// modelos = new TreeSet<>();
		for (Veiculo veiculo : veiculos) {
			modelos.add(veiculo.getModelo());
		}		
		System.out.println("Os modelos cadastrados s�o:");
		for (String modelo : modelos) {
			System.out.println(modelo);
		}
	}

}
