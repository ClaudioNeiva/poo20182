package br.ucsal.bes20182.poo.atividade9;

public class Onibus extends Veiculo {

	private Integer qtdMaximaPassageiros;

	public Onibus(String placa, String modelo, Integer anoFabricacao, Double valor, Integer qtdMaximaPassageiros)
			throws ValorNaoValidoException {
		super(placa, modelo, anoFabricacao, valor);
		this.qtdMaximaPassageiros = qtdMaximaPassageiros;
	}

	@Override
	public Double calcularCustoLocacao() {
		return 10d * qtdMaximaPassageiros;
	}

	public Integer getQtdMaximaPassageiros() {
		return qtdMaximaPassageiros;
	}

	public void setQtdMaximaPassageiros(Integer qtdMaximaPassageiros) {
		this.qtdMaximaPassageiros = qtdMaximaPassageiros;
	}

	@Override
	public String toString() {
		return "Onibus [qtdMaximaPassageiros=" + qtdMaximaPassageiros + ", toString()=" + super.toString() + "]";
	}

}
