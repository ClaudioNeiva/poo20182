package poc.factory;

import java.util.List;

public interface Persistencia {

	void atualizar(Object objeto);
	
	List<Object> consultarTodos();
}
