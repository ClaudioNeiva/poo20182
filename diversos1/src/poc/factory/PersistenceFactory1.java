package poc.factory;

public class PersistenceFactory1 {

	// Alguma propriedade do meu ambiente define o tipo de persistencia
	// Arquivos de configuração
	private static String tipoPersistencia;

	public static Persistencia getPersistencia(){
		if(tipoPersistencia.equalsIgnoreCase("xml")){
			return new PersistencaXML();
		}
		if(tipoPersistencia.equalsIgnoreCase("json")){
			return new PersistencaXML();
		}
		if(tipoPersistencia.equalsIgnoreCase("BD")){
			return new PersistencaXML();
		}
		return null;
	}

}
