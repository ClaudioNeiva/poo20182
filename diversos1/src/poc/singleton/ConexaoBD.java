package poc.singleton;

public class ConexaoBD {

	private static ConexaoBD instance = null;

	private ConexaoBD() {
	}

	public static ConexaoBD getInstance() {
		if (instance == null) {
			instance = new ConexaoBD();
		}
		return instance;
	}

}
