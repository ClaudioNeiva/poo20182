package br.ucsal.bes20181.poo.avaliacao1;

public enum TipoVeiculoEnumVersaoIntermediaria {

	BASICO, INTERMEDIARIO, LUXO;

	public static Double obterValorDiaria(TipoVeiculoEnumVersaoIntermediaria tipo) {
		switch (tipo) {
		case BASICO:
			return 100.45;
		case INTERMEDIARIO:
			return 130.10;
		case LUXO:
			return 156d;
		}
		return null;
	}

}
