package br.ucsal.bes20181.poo.avaliacao1;

import java.util.ArrayList;
import java.util.List;

public class Contrato {

	private static Integer contador = 0;

	private Integer numero;
	private String nomeCliente;
	private String enderecoCliente;
	private List<Veiculo> veiculos = new ArrayList<>();
	private Double valorTotal = 0d;

	public Contrato(String nomeCliente, String enderecoCliente) {
		this.nomeCliente = nomeCliente;
		this.enderecoCliente = enderecoCliente;
		gerarNumeroContrato();
	}

	private void gerarNumeroContrato() {
		contador++;
		this.numero = contador;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public String getEnderecoCliente() {
		return enderecoCliente;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public Boolean adicionarVeiculo(Veiculo veiculo) {
		if (consultarVeiculo(veiculo.getPlaca()) != null) {
			// O ve�culo j� est� na lista (quando pesquisado pela placa).
			return false;
		}
		veiculos.add(veiculo);
		adicionarValorContrato(veiculo.getTipo());
		return true;
	}

	public Boolean removerVeiculo(String placa) {
		Veiculo veiculo = consultarVeiculo(placa);
		if (veiculo == null) {
			// O ve�culo n�o est� na lista (quando pesquisado pela placa).
			return false;
		}
		veiculos.remove(veiculo);
		removerValorContrato(veiculo.getTipo());
		return true;
	}

	public List<Veiculo> consultarVeiculos() {
		return veiculos;
	}

	public List<Veiculo> consultarVeiculosVersao20182(TipoVeiculoEnum tipoVeiculo) {
		List<Veiculo> veiculosSelecionados = new ArrayList<>();
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getTipo().equals(tipoVeiculo)) {
				veiculosSelecionados.add(veiculo);
			}
		}
		return veiculosSelecionados;
	}

	public List<Veiculo> consultarVeiculos(String tipoVeiculo) {
		TipoVeiculoEnum tipoVeiculoEnum = TipoVeiculoEnum.valueOf(tipoVeiculo.toUpperCase());
		List<Veiculo> veiculosSelecionados = new ArrayList<>();
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getTipo().equals(tipoVeiculoEnum)) {
				veiculosSelecionados.add(veiculo);
			}
		}
		return veiculosSelecionados;
	}

	public Veiculo consultarVeiculo(String placa) {
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getPlaca().equalsIgnoreCase(placa)) {
				return veiculo;
			}
		}
		return null;
	}

	private void adicionarValorContrato(TipoVeiculoEnum tipo) {
		valorTotal += tipo.getValorDiaria();
	}

	private void removerValorContrato(TipoVeiculoEnum tipo) {
		valorTotal -= tipo.getValorDiaria();
	}

}
