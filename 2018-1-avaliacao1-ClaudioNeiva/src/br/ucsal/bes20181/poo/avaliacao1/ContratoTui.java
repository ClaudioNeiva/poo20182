package br.ucsal.bes20181.poo.avaliacao1;

public class ContratoTui {

	public static void main(String[] args) {
		Veiculo veiculo1 = new Veiculo("ABC-1234", 2010, TipoVeiculoEnum.BASICO);
		Veiculo veiculo2 = new Veiculo("CDE-7654", 2015, TipoVeiculoEnum.BASICO);
		Veiculo veiculo3 = new Veiculo("XYZ-5151", 2012, TipoVeiculoEnum.LUXO);

		Contrato contrato1 = new Contrato("Maria", "Rua x");
		Contrato contrato2 = new Contrato("Jos�", "Rua y");

		contrato1.adicionarVeiculo(veiculo1);
		contrato1.adicionarVeiculo(veiculo3);

		listarVeiculosContrato(contrato1);

		contrato1.removerVeiculo("ABC-1234");

		listarVeiculosContrato(contrato1);
	}

	private static void listarVeiculosContrato(Contrato contrato) {
		System.out.println("Ve�culos do contrato:");
		for (Veiculo veiculo : contrato.consultarVeiculos()) {
			System.out.println(veiculo);
		}
	}
}
