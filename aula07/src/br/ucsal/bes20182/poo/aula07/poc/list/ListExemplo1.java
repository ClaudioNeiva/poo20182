package br.ucsal.bes20182.poo.aula07.poc.list;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ListExemplo1 {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		List<Object> objetos;

		System.out.println("informe 1 (arraylist) ou 2 (linkedlist):");
		int tipo = scanner.nextInt();

		if (tipo == 1) {
			objetos = new ArrayList<>();
		} else {
			objetos = new LinkedList<>();
		}

		System.out.println("lista criada=" + objetos.getClass().getCanonicalName());

		objetos.add("Claudio");
		objetos.add(123);
		objetos.add(new Date());

		System.out.println("Quantidade de itens na lista: " + objetos.size());
		System.out.println("Segundo objeto = " + objetos.get(1));
		System.out.println("Existe o objeto 123?" + objetos.contains(123));
		System.out.println("Existe o objeto \"Caju\"?" + objetos.contains("Caju"));
		objetos.remove("Claudio");
		System.out.println("objetos=" + objetos);
		objetos.clear();
		System.out.println("Quantidade de itens na lista: " + objetos.size());
	}

}
