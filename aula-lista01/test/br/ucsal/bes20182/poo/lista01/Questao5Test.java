package br.ucsal.bes20182.poo.lista01;

import org.junit.Assert;
import org.junit.Test;

public class Questao5Test {

	@Test
	public void calcularFatorial5Test() {
		// Definir dados de entrada
		Integer n = 5;

		// Definir a sa�da esperada
		Long fatorialEsperado = 120L;

		// Executar o m�todo que est� sendo testado e coletar os resultados
		Long fatorialAtual = Questao5.calcularFatorial(n);

		// Comprar o resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
