package br.ucsal.bes20182.poo.lista01;

import java.util.Scanner;

/**
 * Crie um programa em Java que: a) Leia o nome; b) Leia o sobrenome; c)
 * Concatene o nome com o sobrenome; d) Apresente o nome completo.
 *
 */

public class Questao1 {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		// Declara��o de vari�veis
		String nome;
		String sobrenome;
		String nomeCompleto;

		// Entrada de dados
		System.out.println("Informe o nome:");
		nome = scanner.nextLine();
		System.out.println("Informe o sobrenome:");
		sobrenome = scanner.nextLine();

		// Processamento
		nomeCompleto = nome + " " + sobrenome;

		// Sa�da
		System.out.println("Nome completo=" + nomeCompleto);

	}

}
