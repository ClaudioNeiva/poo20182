package br.ucsal.bes20182.poo.lista01;

import java.util.Scanner;

public class Questao5 {

	public static void main(String[] args) {
		// Declara��o de vari�veis
		Integer n;
		Double e;

		// Entrada
		n = obterNumero();

		// Processamento
		e = calcularE(n);

		// Sa�da
		exibirE(n, e);
	}

	static void exibirE(Integer n, Double e) {
		System.out.println("A soma dos inversos dos fatoriais de zero a " + n + " � " + e);
	}

	static Double calcularE(Integer n) {
		Double e = 0d;
		for (int i = 0; i <= n; i++) {
			e += 1d / calcularFatorial(i);
		}
		return e;
	}

	static Long calcularFatorial(int n) {
		Long fat = 1L;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

	private static Integer obterNumero() {
		Scanner scanner = new Scanner(System.in);
		Integer n;
		while (true) {
			System.out.println("Informe um n�mero maior ou igual � zero:");
			n = scanner.nextInt();
			if (n >= 0) {
				return n;
			} else {
				System.out.println("N�mero n�o v�lido.");
			}
		}
	}

}
