package br.ucsal.bes20182.poo.lista01;

import java.util.Scanner;

/**
 * Suponha que o conceito de um aluno seja determinado em fun��o da sua nota.
 * Suponha, tamb�m, que esta nota seja um valor inteiro na faixa de 0 a 100,
 * conforme a seguinte faixa:
 * 
 * Nota Conceito 0 a 49 Insuficiente 50 a 64 Regular 65 a 84 Bom 85 a 100 �timo
 * 
 * Crie um programa em Java que leia a nota de um aluno e apresente o conceito
 * do mesmo.
 *
 */

public class Questao3 {

	public static void main(String[] args) {
		// Declara��o de vari�veis
		Integer nota;
		String conceito;

		// Entrada
		nota = obterNota();

		// Processamento
		conceito = calcularConceito(nota);

		// Sa�da
		exibirConceito(conceito);
	}

	private static void exibirConceito(String conceito) {
		System.out.println("Conceito = " + conceito);
	}

	private static String calcularConceito(Integer nota) {
		String conceito;
		if (nota <= 49) {
			conceito = "insuficiente";
		} else if (nota <= 64) {
			conceito = "regular";
		} else if (nota <= 84) {
			conceito = "bom";
		} else {
			conceito = "�timo";
		}
		return conceito;
	}

	private static Integer obterNota() {
		Integer nota;
		Scanner scanner = new Scanner(System.in);
		do {
			System.out.println("Informe a nota (0 a 100):");
			nota = scanner.nextInt();
			if (nota < 0 || nota > 100) {
				System.out.println("Nota fora da faixa de valores v�lidos.");
			}
		} while (nota < 0 || nota > 100);
		return nota;
	}

}
