package br.ucsal.bes20182.poo.lista01;

import java.util.Scanner;

public class Questao5Dojo {
	
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		Integer n;
		double fat;
		
		System.out.println("Digite um numero");
		n = sc.nextInt();
		fat = somaFatorial(n);
		
		
		double x = 0;
			x = somaFatorial(n);
		
		System.out.println(x);
		
	}
	
	private static double somaFatorial(Integer n) {
		double resultado = 0;
		
		for (Integer i = 0; i <= n; i++) {
			resultado += 1/calculoFatorial(n-1);
		}
	
		
		return resultado;
	}
	
	private static Integer calculoFatorial(Integer n){
		if( n == 0) {
			return 1;
		} else {
			return n* calculoFatorial(n-1);
		}
	}

}
