package br.ucsal.bes20182.poo.aula11.heranca;

public class ContaCorrenteEspecial extends ContaCorrente {

	private Double limite;

	public ContaCorrenteEspecial(String nome, Double limite) {
		super(nome);
		this.limite = limite;
	}

	// Esse m�todo est� provisoriamente com retorno String, para sinalizar a
	// exce��o que seria a insufici�ncia de saldo. Posteriormente, utilizaremos
	// exception para esta comunica��o.
	@Override
	public String sacar(Double valor) {
		if (saldo + limite < valor) {
			return "saldo insuficiente";
		}
		saldo -= valor;
		return "saque realizado com sucesso";
	}

	public Double getLimite() {
		return limite;
	}

	public void setLimite(Double limite) {
		this.limite = limite;
	}

	@Override
	public String toString() {
		return "ContaCorrenteEspecial [limite=" + limite + ", " + super.toString() + "]";
	}

}
