package br.ucsal.bes20182.poo.aula11.heranca;

public class ContaCorrenteExemplo {

	public static void main(String[] args) {
		sacarContaCorrente();
		sacarContaCorrenteEspecial();
		sacarContaCorrenteDosRicos();
	}

	private static void sacarContaCorrenteDosRicos() {
		System.out.println("***************** CONTA CORRENTE DOS RICOS ***********************");
		ContaCorrenteDosRicos contaCorrenteDosRicos = new ContaCorrenteDosRicos("Antonio Claudio Neiva");
		contaCorrenteDosRicos.depositar(500d);

		ContaCorrenteTui.realizarSaque(contaCorrenteDosRicos, 200000d);
	}
	
	private static void sacarContaCorrenteEspecial() {
		System.out.println("***************** CONTA CORRENTE ESPECIAL ***********************");
		ContaCorrenteEspecial contaCorrenteEspecial = new ContaCorrenteEspecial("Maria da Silda", 200d);
		contaCorrenteEspecial.depositar(500d);

		ContaCorrenteTui.realizarSaque(contaCorrenteEspecial, 200d);
		ContaCorrenteTui.realizarSaque(contaCorrenteEspecial, 400d);
		ContaCorrenteTui.realizarSaque(contaCorrenteEspecial, 150d);
	}

	private static void sacarContaCorrente() {
		System.out.println("***************** CONTA CORRENTE ***********************");
		ContaCorrente contaCorrente = new ContaCorrente("Claudio Neiva");
		contaCorrente.depositar(500d);

		ContaCorrenteTui.realizarSaque(contaCorrente, 200d);
		ContaCorrenteTui.realizarSaque(contaCorrente, 400d);
	}

}
