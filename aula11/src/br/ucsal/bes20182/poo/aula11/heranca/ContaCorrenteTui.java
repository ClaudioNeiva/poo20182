package br.ucsal.bes20182.poo.aula11.heranca;

public class ContaCorrenteTui {

	public static void realizarSaque(ContaCorrente contaCorrente, Double valor) {
		String retorno = contaCorrente.sacar(valor);
		System.out.println("retorno do saque de " + valor + " reais: " + retorno);
		System.out.println(contaCorrente);
	}
}
