package br.ucsal.bes20182.poo.aula11.heranca;

public class ContaCorrenteDosRicos extends ContaCorrente {

	public ContaCorrenteDosRicos(String nome) {
		super(nome);
	}

	@Override
	public String sacar(Double valor) {
		saldo -= valor;
		return "aqui sempre pode sacar!!!!";
	}

}
