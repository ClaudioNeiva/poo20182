package br.ucsal.bes20182.poo.aula11.heranca;

public class ContaCorrente {

	private static Integer seq = 0;

	private Integer numero;

	private String nome;

	protected Double saldo;

	public ContaCorrente(String nome) {
		numerarConta();
		this.nome = nome;
		this.saldo = 0d;
	}

	public void depositar(Double valor) {
		saldo += valor;
	}

	// Esse m�todo est� provisoriamente com retorno String, para sinalizar a
	// exce��o que seria a insufici�ncia de saldo. Posteriormente, utilizaremos
	// exception para esta comunica��o.
	public String sacar(Double valor) {
		if (saldo < valor) {
			return "saldo insuficiente";
		}
		saldo -= valor;
		return "saque realizado com sucesso";
	}

	private void numerarConta() {
		seq++;
		numero = seq;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getNumero() {
		return numero;
	}

	public Double getSaldo() {
		return saldo;
	}

	@Override
	public String toString() {
		return "ContaCorrente [numero=" + numero + ", nome=" + nome + ", saldo=" + saldo + "]";
	}

}
