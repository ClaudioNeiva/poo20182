package br.ucsal.bes20182.poo.aula11.ordenacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrdenacaoGeral {

	public static void main(String[] args) {

		// exibirNomes(10, "claudio", "antonio", "manu", "leila", "pedro");
		// List<String> nomes2 = Arrays.asList("Claudio", "Antonio", "Maria",
		// "Pedro", "Ana", "Joaquim");

		List<String> nomes = new ArrayList<>();

		nomes.add("Claudio");
		nomes.add("Antonio");
		nomes.add("Maria");
		nomes.add("Pedro");
		nomes.add("Ana");
		nomes.add("Joaquim");

		System.out.println("Nomes na ordem que foram informados:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

		Collections.sort(nomes);

		System.out.println("\nNomes em ordem crescente:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

		// O m�todo reverse, como o nome j� indica, apenas inverte a ordem dos
		// elementos na lista. Caso a lista tenha sido previamente ordenada
		// (ordem crescente), a lista resultante do reverse estar� em ordem
		// decrescente.
		Collections.reverse(nomes);

		System.out.println("\nNomes em ordem decrescente:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

	}

	public static void exibirNomes(Integer valor1, String... nomes) {
		System.out.println("valor1=" + valor1);
		for (String nome : nomes) {
			System.out.println(nome);
		}
	}

}
