package br.ucsal.bes20182.poo.aula11.ordenacao;

public class Pessoa implements Comparable<Pessoa> {

	private String nome;

	private Integer anoNascimento;

	private String nomeMae;

	private String email;

	public Pessoa(String nome, Integer anoNascimento, String nomeMae, String email) {
		super();
		this.nome = nome;
		this.anoNascimento = anoNascimento;
		this.nomeMae = nomeMae;
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", anoNascimento=" + anoNascimento + ", nomeMae=" + nomeMae + ", email=" + email
				+ "]";
	}

	// Esta seria uma ordena��o padr�o, e voc� ter� resolver outros crit�rios de
	// ordena��o diretamente no Collections.sort.
	@Override
	public int compareTo(Pessoa outraPessoa) {
		return anoNascimento.compareTo(outraPessoa.anoNascimento);
	}

	public int compareTo_VERSAO1(Pessoa outraPessoa) {
		return (anoNascimento > outraPessoa.anoNascimento) ? 1 : (anoNascimento < outraPessoa.anoNascimento) ? -1 : 0;
	}

	public int compareTo_VERSAO2(Pessoa outraPessoa) {
		if (anoNascimento > outraPessoa.anoNascimento) {
			return 1;
		} else if (anoNascimento < outraPessoa.anoNascimento) {
			return -1;
		}
		return 0;
	}
}
