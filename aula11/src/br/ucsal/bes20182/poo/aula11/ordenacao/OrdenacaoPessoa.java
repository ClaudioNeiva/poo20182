package br.ucsal.bes20182.poo.aula11.ordenacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OrdenacaoPessoa {

	public static void main(String[] args) {
		List<Pessoa> pessoas = new ArrayList<>();
		pessoas.add(new Pessoa("Claudio", 1973, "Lais", "caju@jaca"));
		pessoas.add(new Pessoa("Maria", 1975, "Biana", "manga@goiaba"));
		pessoas.add(new Pessoa("Pedro", 1970, "Lais", "melancia@jaca"));
		pessoas.add(new Pessoa("Joaquim", 1974, "Biana", "uva@jaca"));
		pessoas.add(new Pessoa("Ana", 1971, "Biana", "abacaxi@goiaba"));

		System.out.println("Pessoas, na ordem que foram informadas:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// Caso n�o seja passado o segundo par�metro, a classe dos elementos da
		// lista deve implementar a interface Comparable.
		Collections.sort(pessoas);

		System.out.println("\nPessoas, em ordem crescente de ano de nascimento:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// Voc� pode definir o mecanismo de compara��o atrav�s do segundo
		// par�metro do sort.
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa pessoa1, Pessoa pessoa2) {
				return pessoa1.getNome().compareTo(pessoa2.getNome());
			}
		});

		System.out.println("\nPessoas, em ordem crescente de nome:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		// Aqui temos uma orden��o em n�vel. Primeiro n�vel ser� o nome da m�e e
		// o segundo n�vel o nome da pessoa.
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa pessoa1, Pessoa pessoa2) {
				int resultado = pessoa1.getNomeMae().compareTo(pessoa2.getNomeMae());
				if(resultado == 0){
					resultado = pessoa1.getNome().compareTo(pessoa2.getNome());
				}
				return resultado;
			}
		});

		System.out.println("\nPessoas, em ordem crescente de nome da m�e e como segundo n�mero o nome da pessoa:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

	}

}
