package br.ucsal.bes20182.poo.aula11.excecao;

public class ContaCorrenteExcecaoExemplo {

	public static void main(String[] args) {
		ContaCorrente contaCorrente = new ContaCorrente("Claudio");

		contaCorrente.depositar(500d);

		try {
			contaCorrente.sacar(700d);
		} catch (NegocioException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("saldo=" + contaCorrente.getSaldo());

	}

}
