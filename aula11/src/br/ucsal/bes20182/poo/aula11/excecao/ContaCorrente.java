package br.ucsal.bes20182.poo.aula11.excecao;

public class ContaCorrente {

	private static Integer seq = 0;

	private Integer numero;

	private String nome;

	protected Double saldo;

	public ContaCorrente(String nome) {
		numerarConta();
		this.nome = nome;
		this.saldo = 0d;
	}

	public void depositar(Double valor) {
		saldo += valor;
	}

	public void sacar(Double valor) throws NegocioException {
		if (saldo < valor) {
			throw new NegocioException("Saldo insuficiente.");
		}
		saldo -= valor;
	}

	private void numerarConta() {
		seq++;
		numero = seq;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getNumero() {
		return numero;
	}

	public Double getSaldo() {
		return saldo;
	}

	@Override
	public String toString() {
		return "ContaCorrente [numero=" + numero + ", nome=" + nome + ", saldo=" + saldo + "]";
	}

}
