package br.ucsal.bes.poo20182.avaliacao1;

import java.util.Date;

public class Multa {

	private Date data;

	private String local;

	private TipoMultaEnum tipo;

	public Multa(Date data, String local, TipoMultaEnum tipo) {
		this.data = data;
		this.local = local;
		this.tipo = tipo;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public TipoMultaEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoMultaEnum tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Multa [data=" + data + ", local=" + local + ", tipo=" + tipo + "]";
	}

}
