package br.ucsal.bes.poo20182.avaliacao1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Condutor {

	private Long numeroCnh;

	private String nome;

	private List<Multa> multas = new ArrayList<>();

	public Condutor(Long numeroCnh, String nome) {
		this.numeroCnh = numeroCnh;
		this.nome = nome;
	}

	public void incluirMulta(Date data, String local, TipoMultaEnum tipo) {
		Multa multa = new Multa(data, local, tipo);
		multas.add(multa);
	}

	public List<Multa> consultarMultas(TipoMultaEnum tipoMulta) {
		List<Multa> multasSelecionadas = new ArrayList<>();
		for (Multa multa : multas) {
			if (multa.getTipo().equals(tipoMulta)) {
				multasSelecionadas.add(multa);
			}
		}
		return multasSelecionadas;
	}

	public Integer calcularTotalPontos() {
		Integer totalPontos = 0;
		for (Multa multa : multas) {
			totalPontos += multa.getTipo().getQtdPontos();
		}
		return totalPontos;
	}

	public Long getNumeroCnh() {
		return numeroCnh;
	}

	public void setNumeroCnh(Long numeroCnh) {
		this.numeroCnh = numeroCnh;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
