package br.ucsal.bes.poo20182.avaliacao1;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class CondutorTui {

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	public static void main(String[] args) throws ParseException {
		Condutor condutor1 = new Condutor(21341L, "Jo�o");
		Condutor condutor2 = new Condutor(21341L, "Maria");
		condutor1.incluirMulta(sdf.parse("10/06/2018"), "Pitua�u", TipoMultaEnum.LEVE);
		condutor1.incluirMulta(sdf.parse("12/05/2017"), "Pituba", TipoMultaEnum.GRAVE);
		condutor1.incluirMulta(sdf.parse("11/09/2017"), "Patamares", TipoMultaEnum.GRAVE);

		condutor2.incluirMulta(sdf.parse("10/07/2018"), "Pituba", TipoMultaEnum.LEVE);
		condutor2.incluirMulta(sdf.parse("17/05/2017"), "Pituba", TipoMultaEnum.MEDIA);
		
		System.out.println(condutor1.getNome()+" -> pontos= "+condutor1.calcularTotalPontos());
		System.out.println(condutor2.getNome()+" -> pontos= "+condutor2.calcularTotalPontos());

	}
	
}
