package br.ucsal.bes.poo20182.avaliacao1;

public enum TipoMultaEnum {

	LEVE(3), MEDIA(4), GRAVE(5), GRAVISSIMA(7);

	private Integer qtdPontos;

	private TipoMultaEnum(Integer qtdPontos) {
		this.qtdPontos = qtdPontos;
	}

	public Integer getQtdPontos() {
		return qtdPontos;
	}

}
